import { Component, NgZone } from '@angular/core';
import { BLE } from '@ionic-native/ble/ngx';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage {
	devices: any[] = [];
	password: string;
	ssid: string;
	serviceUUID = 'this should be morad  bluetooth serviceuuid of rpi';
	caracteristicUUID = 'this should be morad caracteristic uuid ';

	constructor(private ble: BLE, private ngZone: NgZone) {}
	onScan() {
		console.log(this.ssid + 'lcode' + this.password);
		this.devices = [];
		this.ble
			.scan([], 15)
			.subscribe((device) => this.onDeviceDiscovered(device));
	}
	onDeviceDiscovered(device) {
		console.log('Discovered' + JSON.stringify(device, null, 2));
		this.ngZone.run(() => {
			this.devices.push(device);
			console.log(device);
		});
	}
	connect(device) {
		// connexion a l'appareille bluetooth
		this.ble.connect(device.id).subscribe(
			(data) => {
				console.log('device connected successfully');
				this.ble.write(
					device.id,
					this.serviceUUID,
					this.caracteristicUUID,
					this.stringToArrayBuffer(this.ssid)
				);
			},
			(data) => {
				console.log('failed to connect');
			}
		);
	}
	getSSID(ssid) {}
	getPassword(password) {}

	stringToArrayBuffer(str) {
		var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
		var bufView = new Uint16Array(buf);
		for (var i = 0, strLen = str.length; i < strLen; i++) {
			bufView[i] = str.charCodeAt(i);
		}
		return buf;
	}
}
function caracteristiUUID(
	id: any,
	serviceUUID: string,
	caracteristiUUID: any,
	arg3: ArrayBuffer
) {
	throw new Error('Function not implemented.');
}
